from django.db import models
from django.utils.safestring import mark_safe

# Create your models here.
class Team(models.Model):
    nombre_team = models.CharField(max_length=50)
    descripcion = models.CharField(max_length=200)
    logo = models.ImageField()
    codigo = models.IntegerField(unique=True)
    coach = models.ForeignKey('Coach', on_delete=models.CASCADE)

    def __str__(self):
        return self.nombre_team

    def logo(self):
        return mark_safe('<img src="/media/%s" style="width:100px"/>' % self.logo)


class Player(models.Model):
    nombre_jugador = models.CharField(max_length=50)
    apodo = models.CharField(max_length=50)
    fecha_nacimiento = models.DateField()
    edad = models.IntegerField()
    rut = models.IntegerField()
    email = models.EmailField()
    estatura = models.IntegerField()
    peso = models.IntegerField()
    foto = models.ImageField(upload_to='pics')
    posicion_juego = models.CharField(max_length=9, choices=(
        ('BASE', 'Base'),
        ('ESCOLTA', 'Escolta'),
        ('ALERO', 'Alero'),
        ('ALA-PIVOT', 'Ala-pivot'),
        ('PIVOTE', 'Pivote')
    ))
    team = models.ForeignKey('Team', on_delete=models.CASCADE)

    def __str__(self):
        return self.nombre_jugador
    
    def foto(self):
        return mark_safe('<img src="/media/%s" style="width:100px"/>' % self.foto)


class Coach(models.Model):
    nombre_coach = models.CharField(max_length=50)
    edad = models.SmallIntegerField()
    email = models.EmailField()
    rut = models.CharField(max_length=11)
    apodo = models.CharField(max_length=50)

    def __str__(self):
        return self.nombre_coach


class Partido(models.Model):
    nombre_partido = models.CharField(max_length=50)

    def __str__(self):
        return self.nombre_partido


class Nomina(models.Model):
    fecha_juego = models.DateField()
    jugadores = models.ManyToManyField('Player')

    def __str__(self):
        return "Nomina  " + str(self.fecha_juego)


    '''
    asdasd
    '''