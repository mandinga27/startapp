from django.contrib import admin

# Register your models here.
from core.models import Team, Player, Partido, Coach, Nomina

#registrando el modelo con un decorador


#@admin.register(Team)
class TeamAdmin(admin.ModelAdmin):
    list_display = ('nombre_team', 'logo')
    search_fields = ['nombre_team',]
admin.site.register(Team, TeamAdmin)

    
#@admin.register(Player)

class PlayerAdmin(admin.ModelAdmin):
    list_display = ('nombre_jugador', 'foto')
    search_fields = ['nombre_jugador', 'apodo', 'rut']
    list_filter = ('fecha_nacimiento', 'team')
admin.site.register(Player, PlayerAdmin)

#@admin.register(Partido)
class PartidoAdmin(admin.ModelAdmin):
    pass
    
#@admin.register(Coach)
class CoachAdmin(admin.ModelAdmin):
    pass
    
#@admin.register(Nomina)
class NominaAdmin(admin.ModelAdmin):
    pass




admin.site.register(Partido)
admin.site.register(Coach)
admin.site.register(Nomina)
